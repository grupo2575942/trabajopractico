using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoAzul : MonoBehaviour
{
    private int hp;
    void Start()
    {
        hp = 100;
    }


    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ProyectilRojo"))
        {
            recibirDaņo();
        }
    }

    public void recibirDaņo()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

}
